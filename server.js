var express = require('express'),
	swig = require('swig'),
	passport = require('passport'),
	session = require('express-session'),
	cookieParser = require('cookie-parser');

var server = express();

/*CONFIG EXPRESS SESSIONS*/
server.use(cookieParser());
server.use(session({secret : 'mi clave'}))
/*PASSPORT CONFIG*/

server.use( passport.initialize());
server.use( passport.session());

passport.serializeUser(function(user, done){
	done(null, user); //req.user 
})

passport.deserializeUser(function(user, done){
	done(null, user); //req.user 
})

/*SWIG CONFIG*/

server.engine('html', swig.renderFile);
server.set('view engine', 'html');
server.set('views', __dirname + '/app/views');

/* STATIC FILES */
server.use(express.static('./public'));

require('./app/controllers/home')(server);

server.listen(8000);